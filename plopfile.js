module.exports = function (plop) {
  plop.setHelper("ifEquals", function (arg1, arg2, options) {
    return (arg1 == arg2) ? options.fn(this) : options.inverse(this);
  });

  plop.setGenerator("React Component", {
    description: "Create a basic react component",
    prompts: [
      {
        type: "input",
        name: "dir",
        message: "Starting from the src/components dir, enter the path",
      },
      {
        type: "input",
        name: "name",
        message: "Component name?",
      },
    ],
    actions: [
      {
        type: "add",
        path: "src/components/{{dir}}/{{pascalCase name}}/index.tsx",
        templateFile: "plop-templates/Component.js.hbs",
      },
      // {
      //   type: "add",
      //   path: "src/components/{{dir}}/{{pascalCase name}}/styles.css",
      //   templateFile: "plop-templates/Styles.css.hbs",
      // },
    ],
  });

  plop.setGenerator("React Hook", {
    description: "Create a basic react hook",
    prompts: [
      {
        type: "input",
        name: "dir",
        message: "Starting from the src/hooks dir, enter the path",
      },
      {
        type: "input",
        name: "name",
        message: "Hook name?",
      },
    ],
    actions: [
      {
        type: "add",
        path: "src/hooks/{{dir}}/{{pascalCase name}}.ts",
        templateFile: "plop-templates/Hook.js.hbs",
      },
    ],
  });
};
