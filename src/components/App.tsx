import Board from "./Board";

function App() {
  return (
    <div className="flex flex-col items-center justify-center w-full gap-8 p-8">
      <h1 className="text-4xl font-bold">Tic Tac Toe</h1>
      {/* <Status /> */}
      <Board />
    </div>
  );
}

export default App;
