import { useReducer } from "react"

const WINNING_SQUARES = [
  [0, 1, 2],
  [3, 4, 5],
  [6, 7, 8],
  [0, 3, 6],
  [1, 4, 7],
  [2, 5, 8],
  [0, 4, 8],
  [2, 4, 6]
]

export const enum PlayerType {
  Player = "Player",
  Computer = "Computer"
}

export const enum Mark {
  Naught = "O",
  Cross = "X",
  None = ""
}

type State = ({
  status: "player-turn" | "computer-turn" | "reset"
} | {
  status: "game-over"
  winner: PlayerType | undefined
}) & {
  squares: Mark[]
  turnCount: number
}

type Action =
  | { type: "player-turn", squareIndex: number }
  | { type: "computer-turn" }
  | { type: "next-game", firstMove: PlayerType }
  | { type: "reset" }

export function useBoard() {
  const [state, next] = useReducer(reducer, reset())

  return {
    state,
    computerTurn: () => next({ type: "computer-turn" }),
    playerTurn: (squareIndex: number) => next({ type: "player-turn", squareIndex }),
    nextGame: (firstMove: PlayerType) => next({ type: "next-game", firstMove }),
    reset: () => next({ type: "reset" })
  }
}

function getEmptyBoard() {
  return Array.from(Array(9).keys()).map(() => Mark.None)
}

function isWinner(squares: Mark[]) {
  for (let i = 0; i < WINNING_SQUARES.length; i++) {
    const winningSquares = WINNING_SQUARES[i]
    if (squares[winningSquares[0]] === squares[winningSquares[1]]
      && squares[winningSquares[0]] !== Mark.None
      && squares[winningSquares[1]] === squares[winningSquares[2]]
      && squares[winningSquares[2]] !== Mark.None) {
      return true
    }
  }
  return false
}

function reset(): State {
  return {
    status: "player-turn",
    squares: getEmptyBoard(),
    turnCount: 0
  }
}

function randomSquare() {
  return Math.floor(Math.random() * 9)
}

function reducer(previous: State, action: Action): State {
  switch (action.type) {
    case "computer-turn": {
      const updatedSquares = [...previous.squares]
      while (true) {
        const compIndex = randomSquare()
        if (updatedSquares[compIndex] === Mark.None) {
          updatedSquares[compIndex] = Mark.Naught
          break
        }
      }
      const winner = isWinner(updatedSquares)
      return {
        status: winner || previous.turnCount === 8 ? "game-over" : "player-turn",
        winner: winner ? PlayerType.Computer : undefined,
        squares: updatedSquares,
        turnCount: previous.turnCount + 1
      }
    }
    case "player-turn": {
      const updatedSquares = [...previous.squares]
      updatedSquares[action.squareIndex] = Mark.Cross
      const winner = isWinner(updatedSquares)
      return {
        status: winner || previous.turnCount === 8 ? "game-over" : "computer-turn",
        winner: winner ? PlayerType.Player : undefined,
        squares: updatedSquares,
        turnCount: previous.turnCount + 1
      }
    }
    case "next-game": {
      return {
        status: action.firstMove === PlayerType.Player ? "player-turn" : "computer-turn",
        squares: getEmptyBoard(),
        turnCount: 0
      }
    }
    case "reset": {
      return reset()
    }
    default:
      return previous
  }
}