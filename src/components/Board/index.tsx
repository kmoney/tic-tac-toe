import React, { useEffect } from "react";
import clsx from "clsx";
import { Mark, PlayerType, useBoard } from "./state";

export type BoardProps = {
} & React.HTMLAttributes<HTMLDivElement>

function Board({
  className,
  ...rest
}: BoardProps) {
  const { state: board, playerTurn, computerTurn, nextGame, reset } = useBoard()

  useEffect(() => {
    if (board.status === "computer-turn") {
      computerTurn()
    }
  }, [board])
  return (
    <div className={clsx(className, "flex flex-col items-center gap-8")} {...reset}>
      <h2 className="text-2xl">Turns: {board.turnCount}</h2>
      {board.status === "game-over" && (
        <h3 className="text-4xl">Winner: {board.winner || "No one!"}</h3>
      )}
      <div className="grid grid-cols-3">
        {board.squares.map((square, index) => {
          return (
            <div
              key={`square-${index}`}
              className={clsx(
                "flex flex-col items-center justify-center border-2 border-black w-20 h-20 text-5xl",
                index % 3 === 0 && "border-l-0",
                index < 3 && "border-t-0",
                index % 3 === 2 && "border-r-0",
                index > 5 && "border-b-0"
              )}
              onClick={() => {
                if (board.status !== "game-over" && square === Mark.None) {
                  playerTurn(index)
                }
              }}
            >
              {square}
            </div>
          )
        })}
      </div>
      <button
        className="border-2 border-black rounded-lg p-4 font-bold hover:bg-black hover:border-gray-500 hover:text-white"
        onClick={() => {
          if (board.status === "game-over") {
            nextGame(board.winner as PlayerType === PlayerType.Computer ? PlayerType.Player : PlayerType.Computer)
          } else {
            reset()
          }
        }}>New Game</button>
    </div>
  )
}

export default Board;